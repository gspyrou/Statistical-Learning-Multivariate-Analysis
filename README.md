# STATS-503---Statistical-Learning-Multivariate-Analysis
Projects for the course STATS 503 

1) **MDS_map.r**

    Produces a 2-dimensional map between points , given a matrix of distances

2) **fa_multiscaling_crabs.r**

    Factor Analysis and Multidimensional Scaling on Crabs dataset

3) **email_classification.r**

    We contstruct different classification rules to predict the type of an email given some email features.
    For this task we implement Linear Discriminant Analysis(LDA) , Quadratic Discriminant Analysis (QDA) , Logistic Regression
    and kNN.

4) **email_spam_classif_ML.r**

    Again we are working with the email spam dataset but now we perform prediction using Machine Learning methods.
    Specifically we are using Support Vector Machines and Neural Networks in order to predict the type of the email (Spam - Not Spam)

5) **clustering_crabs.r**

    Clustering analysis using on crabs dataset,using Hierarchical Clustering, K-means and Mixture modeling.
