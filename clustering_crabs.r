
library(cluster)
library(dplyr)
library(data.table)
library(ggplot2)
library(gridExtra)
require(ggdendro)
library(ggthemes)
library(matrixStats)
library(flexclust)
require(mixtools)

#  we are loading and reforming the proper file
data_crabs<- read.table("crabs.txt",header=TRUE) # loading the file

# we dont need sex and species so delete the first two columns
data_crabs_reduced = data_crabs[,-c(1,2)]
data_scaled = scale(data_crabs_reduced) # we scale the data ??
dis = dist(data_scaled)
mds = as.data.frame(cmdscale(dis, k=2))

# visualize the data
q1 = ggplot(mds, aes(x=V2, y=V1)) + geom_point(alpha=0.6)
q2 = ggplot(data_crabs_reduced,aes(x=FL, y=RW)) + geom_point(alpha=0.6)
q3 = ggplot(data_crabs_reduced, aes(x=CL, y=BD)) + geom_point(alpha=0.6)
q4 = ggplot(data_crabs_reduced, aes(x=FL, y=CL)) + geom_point(alpha=0.6)
grid.arrange(q1, q2, q3, q4, ncol=2)

# now we have to perform Hierachical Clustering Method 

crabs_single = agnes(data_scaled, diss=FALSE, method='single') # first method: "Single" linkage 
crabs_complete= agnes(data_scaled, diss=FALSE, method='complete')# second method : "Complete" linkage
crabs_ward= agnes(data_scaled, diss=FALSE, method='ward')# third method : "ward" method

# now lets see the silhouettes
# with euclidean / i should build a loop for different k's
sil_single = silhouette(cutree(crabs_single, k=4), dis)%>%plot(,main="Silhouette for Single Linkage") 
sil_complete = silhouette(cutree(crabs_complete, k=4), dis)%>%plot(,main="Silhouette for Complete")
sil_ward = silhouette(cutree(crabs_ward, k=4), dis)%>%plot(, main="Silhouette for Ward")

summary(sil_single)
summary(sil_complete)
summary(sil_ward)

#dendrograms
dendro_sigle = ggdendrogram(as.dendrogram(crabs_single), leaf_labels=FALSE, labels=FALSE )
dendro_sigle

dendro_complete = ggdendrogram(as.dendrogram(crabs_complete), leaf_labels=FALSE, labels=FALSE )
dendro_complete

dendro_ward = ggdendrogram(as.dendrogram(crabs_ward), leaf_labels=FALSE, labels=FALSE )
dendro_ward

#########lets try for different k's
k_values = c(2,3,5,6)
sapply(k_values,function(x) plot(silhouette(cutree(crabs_single, k=x),dist = dis))) # for single
sapply(k_values,function(x) plot(silhouette(cutree(crabs_complete, k=x),dist = dis))) # complete 
sapply(k_values,function(x) plot(silhouette(cutree(crabs_ward, k=x),dist = dis),main="Silhouette for Ward")) # wards

summary(silhouette(cutree(crabs_ward , k = 2),  dist = dis))
summary(silhouette(cutree(crabs_ward , k = 3),  dist = dis))
summary(silhouette(cutree(crabs_ward , k = 4),  dist = dis))

#########part b
## we have to use sex and species variables too

data_crabs$full_clust = NA

data_crabs[data_crabs$Sex==1 & data_crabs$Species ==1 , "full_clust"] = 1
data_crabs[data_crabs$Sex==1 & data_crabs$Species ==2 , "full_clust"] = 2
data_crabs[data_crabs$Sex==2 & data_crabs$Species ==1 , "full_clust"] = 3
data_crabs[data_crabs$Sex==2 & data_crabs$Species ==2 , "full_clust"] = 4

data_crabs$full_clust =as.factor(data_crabs$full_clust)

pca = data.frame(t(as.matrix(prcomp(data_crabs_reduced)$rotation) %*% t(as.matrix(data_crabs_reduced))))
# whole
#1st set
ggg1 = ggplot(data_crabs,aes(x=FL,y=CW,color=full_clust))+geom_point()+theme_bw()+ 
  labs(colour = "Cluster")+labs(title = "Plot using Sex&Species variables") 
#2nd set
ggg2 = ggplot(data_crabs,aes(x=BD,y=RW,color=full_clust))+geom_point()+theme_bw()+
  labs(colour = "Cluster")+labs(title = "Plot using Sex&Species variables") 

grid.arrange(ggg1,ggg2,ncol=2)

### using pca findings
all = cbind(pca, zz =data_crabs$full_clust)
gg1 = ggplot(all,aes(x=FL,y=CW,color=zz))+geom_point()+theme_bw()+ labs(colour = "Cluster")+
  labs(title = "Plot using Sex&Species \nvariables / PCA")
gg2 = ggplot(all,aes(x=BD,y=RW,color=zz))+geom_point()+theme_bw()+ labs(colour = "Cluster")+
  labs(title = "Plot using Sex&Species \nvariables / PCA")

grid.arrange(gg1,gg2,ncol=2)

##### K-MEANS analysis

kpp_init = function(dat, K) {
  x = as.matrix(dat)
  n = nrow(x)
  
  # Randomly choose a first center
  centers = matrix(NA, nrow=K, ncol=ncol(x))
  centers[1,] = as.matrix(x[sample(1:n, 1),])
  
  for (k in 2:K) {
    # Calculate dist^2 to closest center for each point
    dists = matrix(NA, nrow=n, ncol=k-1)
    for (j in 1:(k-1)) {
      temp = sweep(x, 2, centers[j,], '-')
      dists[,j] = rowSums(temp^2)
    }
    dists = rowMins(dists)
    
    # Draw next center with probability propor to dist^2
    cumdists = cumsum(dists)
    prop = runif(1, min=0, max=cumdists[n])
    centers[k,] = as.matrix(x[min(which(cumdists > prop)),])
  }
  return(centers)
}
#### change it for different k's // we will use k=2,3,4,5
set.seed(123)
clust_kpp = kmeans(data_scaled, kpp_init(data_scaled, 5), iter.max=100, algorithm='Lloyd')
set.seed(12345)
clust_kpp2 = kmeans(data_scaled, kpp_init(data_scaled, 5), iter.max=100, algorithm='Lloyd')

mds_temp = cbind(
  mds, as.factor(clust_kpp$cluster), as.factor(clust_kpp2$cluster))
names(mds_temp) = c('V1', 'V2', 'clust1', 'clust2')

mds_temp = cbind(data_crabs_reduced,mds_temp)

gp1 = ggplot(mds_temp, aes(x=CW, y=FL, color=clust1)) +
  geom_point() +theme_dark()+ labs(colour = "Cluster") 
gp2 = ggplot(mds_temp, aes(x=RW, y=BD, color=clust2)) +
  geom_point() +theme_dark()+ labs(colour = "Cluster") 
grid.arrange(gp1, gp2, ncol=2,top = "For k = 5")

####### mixture models analyis
data_crabs<- read.table("crabs.txt",header=TRUE)
data_crabs_reduced = data_crabs[,-c(1,2)]
data_scaled = scale(data_crabs_reduced) # we scale the data ??

set.seed(12345)
gmm = mvnormalmixEM(as.matrix(data_crabs_reduced), k=5)
kmeans = kmeans(data_crabs_reduced, 5)

data_crabs_reduced$gmm = as.factor(apply(gmm$posterior, 1, which.max))
data_crabs_reduced$kmeans = as.factor(kmeans$cluster)

crabs_FL_CW = data_crabs_reduced[,c(1,4,6,7)]
x_melt = melt(crabs_FL_CW, id.vars=c('FL', 'CW'))

q1 = ggplot(x_melt, aes(x=FL, y=CW , color=value)) +
  geom_point() + facet_grid(~variable) + theme(legend.position='none')+theme_gdocs()+ labs(colour = "Cluster")

q1

###################################################################################
data_crabs<- read.table("crabs.txt",header=TRUE)
data_crabs_reduced = data_crabs[,-c(1,2)]
data_scaled = scale(data_crabs_reduced) # we scale the data ??

require("mixtools")
set.seed(12345)
gmm = mvnormalmixEM(as.matrix(data_crabs_reduced), k=5)
kmeans = kmeans(data_crabs_reduced, 5)

data_crabs_reduced$gmm = as.factor(apply(gmm$posterior, 1, which.max))
data_crabs_reduced$kmeans = as.factor(kmeans$cluster)

crabs_RW_BD = data_crabs_reduced[,c(2,5,6,7)]
x_melt = melt(crabs_RW_BD, id.vars=c('RW', 'BD'))

q2 = ggplot(x_melt, aes(x=RW, y=BD , color=value)) +
  geom_point() + facet_grid(~variable) + theme(legend.position='none')+theme_gdocs()+labs(colour = "Cluster")

q2

##############################################
#lets try the previous but now for the PCA data
data_crabs<- read.table("crabs.txt",header=TRUE) # loading the file
data_crabs_reduced = data_crabs[,-c(1,2)]
data_scaled = scale(data_crabs_reduced)
pca = data.frame(t(as.matrix(prcomp(data_crabs_reduced)$rotation) %*% t(as.matrix(data_crabs_reduced))))

set.seed(12345)
gmm = mvnormalmixEM(as.matrix(pca), k=3)
kmeans = kmeans(pca,3)

pca$gmm = as.factor(apply(gmm$posterior, 1, which.max))
pca$kmeans = as.factor(kmeans$cluster)

crabs_FL_CW = pca[,c(1,4,6,7)]
x_melt = melt(crabs_FL_CW, id.vars=c('FL', 'CW'))
q1 = ggplot(x_melt, aes(x=FL, y=CW , color=value)) +
  geom_point() + facet_grid(~variable) + theme(legend.position='none')+theme_gdocs()+ labs(colour = "Cluster")

q1
##############################################
# we have to find the optimal value for k
# so we will apply the BIC criterion
library(mclust)
results_BIC = mclustBIC(data_scaled)
plot(results_BIC,main = "Plot for BIC")
summary(results_BIC)
