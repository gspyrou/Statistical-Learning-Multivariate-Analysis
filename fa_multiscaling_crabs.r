
library(data.table)
library(nFactors)
library(cluster)
library(dplyr)
library(ggplot2)
library(gridExtra)

#  we are loading and reforming the proper file

data_crabs<- read.table("crabs.txt",header=TRUE) # loading the file
data_crabs<-as.data.frame(data_crabs)      # i like using the data in data frame format 
dim(data_crabs) #  214 7 # dimension of our data
head(data_crabs)
# starting our task- we are using data_crabs

# FACTOR ANALYSIS first

# at first we need to see which variables we are going to include
# we will not use sex and species , as it makes no sense to use them in a factor analysis (same as PCA that we did)
# in homework 1

mydata<-data_crabs[,-c(1,2)] # we dont need the first two columns

cov_matrix<-cov(mydata) # covariance matrix of our data

cor_matrix<-cov2cor(cov_matrix) # correlation matrix for our data

factor_analysis <- factanal(covmat = cov_matrix,factors = 2) # factor analysis for this set of data
print(factor_analysis)


# we can see that some correlations are high / so we can do Factor Analysis
# first we have to see how many factors we want

evall <- eigen(cor_matrix)
ap <- parallel(subject=nrow(mydata),var=ncol(mydata),
               rep=100,cent=.05)

nS <- nScree(x=evall$values, aparallel=ap$eigen$qevpea)
plotnScree(nS) # so we need to use 2 factors

# 2-factor model-promax rotation
ability.FA.promax = factanal(factors=2, covmat=cov_matrix,  # same task but with the promax rotation
                             rotation="promax")

ability.FA.promax # results with the promax rotation 

# and the scores for this issue
fa_scores = factanal(mydata, factors=2, scores='regression')
scores = fa_scores$scores
df = data.frame(Factor1=scores[,1], Factor2=
                  scores[,2])

ggplot(df,aes(x=Factor1,y=Factor2))+labs(title="Scores Plot - Factor Analysis")+geom_text(aes(label=rownames(df)))
theme_bw() + ggtitle("Factor Analysis Scores")

# Now we will work with the Multidimensional scaling analysis

# here is different because we can actually use the categorical / ordinal variables
# something we couldnt do it pca or fa 

data_crabs_scaled<-scale(data_crabs)
the_distance<-dist(data_crabs_scaled,method = "euclidean")
mds = cmdscale(the_distance, k=2)

qplot(x=mds[,1], y = mds[,2], label=rownames(data_crabs), 
      geom="text")+labs(title="MDS 1 vs MDS 2",x="MDS 1",y="MDS 2")

#  mds vs pca analysis

data_crabs_2<-data_crabs[,-1:-2]  # pc analysis as in homework 1
mydata_scaled<-scale(data_crabs_2)
pca = princomp(mydata_scaled)

p_mds = qplot(x=mds[,1], y = mds[,2], label=rownames(data_crabs), 
              geom="text", main = "MDS")+labs(x="MDS 1",y="MDS 2")+theme_bw()
p_pca = qplot(x=pca$scores[,1], y = pca$scores[,2], label=rownames(data_crabs_2), 
              geom="text", main = "PCA")+labs(x="PCA Scores 1",y="PCA Scores 2")+theme_bw()

grid.arrange(p_mds, p_pca, ncol=2) # so we can compare these two different methods

# fa vs pca 

p_fa<-qplot(x=df$Factor1,y=df$Factor2,label=row.names(mydata),
            geom='text')+labs(title="Scores Plot - Factor Analysis",
            x="Factor 1",y='Factor 2')+theme_bw()

grid.arrange(p_fa, p_pca, ncol=2) # so we can compare these two different methods

