
library(dplyr)
library(data.table)
library(ggplot2)
library(GGally)
library(MASS)
library(ISLR)
library(sparsediscrim)
library(e1071)
library(reshape2)
require(nnet)     
require(stats)
require(class)
library(gridExtra)
require(neuralnet)
library(spatstat)

testdata <-read.table("spam-test.txt",sep=",")  # 1534 obs 
traindata <-read.table("spam-train.txt",sep=",") # 3067 obs

# we are goint to work with the standardized data for this homework
# standardized data
testdata2 <-testdata[,-58]       # there was also another column number 58 which had only 0's and 1's so
traindata2<-traindata[,-58]      # we can discard this column

testdata_centered <- as.data.frame(scale(testdata2, center = TRUE, scale = TRUE))
traindata_centered <- as.data.frame(scale(traindata2, center = TRUE, scale = TRUE))

testdata_centered$V58 = testdata[,58]
traindata_centered$V58 = traindata[,58]

testdata$V58 = as.factor(testdata$V58)
traindata$V58 = as.factor(traindata$V58)

testdata_centered$V58[testdata_centered$V58==1] = "Spam"
testdata_centered$V58[testdata_centered$V58==0] = "No Spam"

traindata_centered$V58[traindata_centered$V58==1]= "Spam"
traindata_centered$V58[traindata_centered$V58==0]= "No Spam"

testdata_centered$V58 = as.factor(testdata_centered$V58)
traindata_centered$V58 = as.factor(traindata_centered$V58)

# Part A - Support Vector Machines (SVM)
traindata.svm.linear <- svm(V58~., data=traindata_centered,
                            kernel="linear", cost=1) # using linear kernel
summary(traindata.svm.linear)

table(testdata_centered$V58, predict(traindata.svm.linear, testdata_centered)) # table with 0,1 's

sum(predict(traindata.svm.linear, testdata_centered) != testdata_centered$V58)/nrow(testdata_centered) # 0.07170795

# now we are using polynomial kernel
traindata.svm.polynomial <- svm(traindata_centered$V58~., data=traindata_centered,
                                kernel="polynomial", cost=1, degree = 3) # using polynomial kernel

summary(traindata.svm.polynomial) # summary using polynomial kernel

table(testdata_centered$V58, predict(traindata.svm.polynomial, testdata_centered)) # table with 0,1 's

sum(predict(traindata.svm.polynomial, testdata_centered) != testdata_centered$V58)/nrow(testdata_centered) # 0.214472

# now we are using gaussian kernels
traindata.svm.gaussian <- svm(traindata_centered$V58~., kernel="radial", data=traindata_centered, cost=50) # using gaussian kernel

summary(traindata.svm.gaussian) # summary using gaussian kernel

table(testdata_centered$V58, predict(traindata.svm.gaussian, testdata_centered)) # table with 0,1 's

sum(predict(traindata.svm.gaussian, testdata_centered) != testdata_centered$V58)/nrow(testdata_centered) # 0.06453716

# best tunning parameter
costs = exp(-5:8)
tune_poly = tune.svm(V58~., data=traindata_centered ,  
                     cost = costs , kernel = "polynomial") # best cost =  403.4288/  best performance = 0.05151583
summary(tune_poly)
# best tunning parameter
tune_linear = tune.svm(V58~., data=traindata_centered ,  
                       cost = costs, kernel = "linear") # best cost =  7.38905  /  best performance = 0.05802942
summary(tune_linear)
# using the optimal values for cost tha we found

#linear
traindata.svm.linear <- svm(V58~., data=traindata_centered,
                            kernel="linear", cost=7.38905) # using linear kernel
summary(traindata.svm.linear)
table(testdata_centered$V58, predict(traindata.svm.linear, testdata_centered)) 

#polynomial
traindata.svm.polynomial <- svm(traindata_centered$V58~., data=traindata_centered,
                                kernel="polynomial", cost=403.4288, degree = 3) # using polynomial kernel

summary(traindata.svm.polynomial) # summary using polynomial kernel
table(testdata_centered$V58, predict(traindata.svm.polynomial, testdata_centered)) 

#gaussian
traindata.svm.gaussian <- svm(traindata_centered$V58~., 
                              kernel="radial", data=traindata_centered, cost=54.59815) # using gaussian kernel
summary(traindata.svm.gaussian) # summary using gaussian kernel
table(testdata_centered$V58, predict(traindata.svm.gaussian, testdata_centered)) 

# now checking for the sensitivity according to more parameters
# we are going to change some of them

#polynomial - we are changing the degrees and the coef for the polynomial

costs = exp(-2:4)
tune_poly = tune.svm(V58~., data = traindata_centered, cost = costs, kernel = "polynomial", 
                     degree = c(1:3), coef0 = c(0.2:3))
summary(tune_poly)

# radial -  here we are changing the value of gamma
tune_radial = tune.svm(V58~., data = traindata_centered, cost = costs, kernel = "radial",
                       gamma = c(0.1:3))
summary(tune_radial)

### PART B - Neural Nets - one layer
testdata <-read.table("spam-test.txt",sep=",")  # 1534 obs 
traindata <-read.table("spam-train.txt",sep=",") # 3067 obs
set.seed(123)

pred_mse = function(nn, bos_dat) {
  yhat = compute(nn, bos_dat[,-58])$net.result
  sum((yhat - bos_dat[58])^2)/nrow(bos_dat)
}

testdata2 = scale(testdata[,1:57])
traindata2 = scale(traindata[,1:57])
testdata_standardized = cbind(testdata2,testdata$V58)%>%as.data.frame()
traindata_standardized = cbind(traindata2,traindata$V58)%>%as.data.frame()

# one hidden layer - 49 hidden nodes
nn_single_1 = neuralnet(
  paste('V58','~', paste(colnames(traindata_standardized[,1:57]), collapse='+')),
  traindata_standardized,
  hidden=58,
  linear.output=FALSE,
  lifesign='full', lifesign.step=100)

pred_mse(nn_single_1, traindata_standardized) # 0.002064137405
pred_mse(nn_single_1, testdata_standardized) #   0.07427119537
plot(nn_single_1)

# one hidden layer - 200 hidden nodes
nn_single_2 = neuralnet(
  paste('V58','~', paste(colnames(traindata_standardized[,1:57]), collapse='+')),
  traindata_standardized,
  hidden=200,
  linear.output=FALSE,
  lifesign='full', lifesign.step=100)

pred_mse(nn_single_2, traindata_standardized) # 0.008910538638
pred_mse(nn_single_2, testdata_standardized) # 0.04155502676
plot(nn_single_2)

# two hidden layers - 58 and 60 nodes 
nn_mult_1 = neuralnet(
  paste('V58','~', paste(colnames(traindata_standardized[,1:57]), collapse='+')),
  traindata_standardized,
  hidden=c(58,60),
  linear.output=FALSE,
  lifesign='full', lifesign.step=100) 

pred_mse(nn_mult_1, traindata_standardized) # 0.001699861924

pred_mse(nn_mult_1, testdata_standardized) # 0.03551941702

# two hidden layers - 58 and 200 nodes 
nn_mult_2 = neuralnet(
  paste('V58','~', paste(colnames(traindata_standardized[,1:57]), collapse='+')),
  traindata_standardized,
  hidden=c(58,200),
  linear.output=FALSE,
  lifesign='full', lifesign.step=100) 

pred_mse(nn_mult_2, traindata_standardized) # 0.005889978773

pred_mse(nn_mult_2, testdata_standardized) #  0.06487091523

### PART C - errors

# Linear
set.seed(1071)
folds = cv_partition(traindata_centered$V58, num_folds = 5)

train_cv_error_svm = function(costC) {
  #Train
  auto.svm = svm(traindata_centered$V58~., data=traindata_centered,
                 kernel="linear", cost=costC)
  train_error = sum(auto.svm$fitted != traindata_centered$V58) / nrow(traindata_centered)
  #Test
  test_error = sum(predict(auto.svm, testdata_centered) != testdata_centered$V58) / nrow(testdata_centered)
  #CV error
  autoe.cverr = sapply(folds, function(fold) {
    svmcv = svm(traindata_centered$V58~.,data = traindata_centered, kernel="linear", cost=costC, subset = fold$training)
    svmpred = predict(svmcv, traindata_centered[fold$test,])
    return(sum(svmpred != traindata_centered$V58[fold$test]) / length(fold$test))
  })
  cv_error = mean(autoe.cverr)
  return(c(train_error, cv_error, test_error))
}

costs = exp(-2:2)
data_cost_errors = sapply(costs, function(cost) train_cv_error_svm(cost))
df_errs = data.frame(t(data_cost_errors), costs)
colnames(df_errs) = c('Train', 'CV', 'Test', 'Logcost')


dataL <- melt(df_errs, id="Logcost")
ggplot(dataL, aes_string(x="Logcost", y="value", colour="variable",
                         group="variable", linetype="variable", shape="variable")) +
  geom_line(size=1) + labs(title="Classification Error-Cost parameter plot / Linear",
                           x = "Cost",
                           y = "Classification error",
                           colour="",group="",
                           linetype="",shape="") + scale_x_log10()

# polynomial kernel

set.seed(1071)
folds = cv_partition(traindata_centered$V58, num_folds = 5)

train_cv_error_svm = function(costC) {
  #Train
  auto.svm = svm(traindata_centered$V58~., data=traindata_centered,
                 kernel="polynomial", cost=costC)
  train_error = sum(auto.svm$fitted != traindata_centered$V58) / nrow(traindata_centered)
  #Test
  test_error = sum(predict(auto.svm, testdata_centered) != testdata_centered$V58) / nrow(testdata_centered)
  #CV error
  autoe.cverr = sapply(folds, function(fold) {
    svmcv = svm(traindata_centered$V58~.,data = traindata_centered, kernel="polynomial", cost=costC, subset = fold$training)
    svmpred = predict(svmcv, traindata_centered[fold$test,])
    return(sum(svmpred != traindata_centered$V58[fold$test]) / length(fold$test))
  })
  cv_error = mean(autoe.cverr)
  return(c(train_error, cv_error, test_error))
}

costs = exp(-5:8)
data_cost_errors = sapply(costs, function(cost) train_cv_error_svm(cost))
df_errs = data.frame(t(data_cost_errors), costs)
colnames(df_errs) = c('Train', 'CV', 'Test', 'Logcost')


dataL <- melt(df_errs, id="Logcost")
ggplot(dataL, aes_string(x="Logcost", y="value", colour="variable",
                         group="variable", linetype="variable", shape="variable")) +
  geom_line(size=1) + labs(title="Classification Error-Cost parameter plot / Polynomial",x = "Cost",
                           y = "Classification error",
                           colour="",group="",
                           linetype="",shape="") + scale_x_log10()

# Gaussian
# radial kernel

set.seed(1071)
folds = cv_partition(traindata_centered$V58, num_folds = 5)

train_cv_error_svm = function(costC) {
  #Train
  auto.svm = svm(traindata_centered$V58~., data=traindata_centered,
                 kernel="radial", cost=costC)
  train_error = sum(auto.svm$fitted != traindata_centered$V58) / nrow(traindata_centered)
  #Test
  test_error = sum(predict(auto.svm, testdata_centered) != testdata_centered$V58) / nrow(testdata_centered)
  #CV error
  autoe.cverr = sapply(folds, function(fold) {
    svmcv = svm(traindata_centered$V58~.,data = traindata_centered, kernel="radial", cost=costC, subset = fold$training)
    svmpred = predict(svmcv, traindata_centered[fold$test,])
    return(sum(svmpred != traindata_centered$V58[fold$test]) / length(fold$test))
  })
  cv_error = mean(autoe.cverr)
  return(c(train_error, cv_error, test_error))
}

costs = exp(-2:6)
data_cost_errors = sapply(costs, function(cost) train_cv_error_svm(cost))
df_errs = data.frame(t(data_cost_errors), costs)
colnames(df_errs) = c('Train', 'CV', 'Test', 'Logcost')


dataL <- melt(df_errs, id="Logcost")
ggplot(dataL, aes_string(x="Logcost", y="value", colour="variable",
                         group="variable", linetype="variable", shape="variable")) +
  geom_line(size=1) + labs(title= "Classification Error-Cost parameter plot / Gaussian",x = "Cost",
                           y = "Classification error",
                           colour="",group="",
                           linetype="",shape="") + scale_x_log10()

