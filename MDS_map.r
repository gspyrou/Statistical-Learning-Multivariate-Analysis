

library(ggplot2)

# i chose to build a function for the renaming of the matrices during the problem
# as it would be better for display and a little for challenging
rename_func<- function(x){  
  
  x<-as.matrix(x)     # transforming it into a matrix form
  
  dim(x)=c(10,10) 
  colnames(x)  = c("ATLANTA","CHICAGO","DENVER","HOUSTON",
                   "LOS ANGELES","MIAMI","NEW YORK","SAN FRANCISCO","SEATTLE","WASHINGTON DC")
  rownames(x)  = c("ATLANTA","CHICAGO","DENVER","HOUSTON",
                   "LOS ANGELES","MIAMI","NEW YORK","SAN FRANCISCO","SEATTLE","WASHINGTON DC")
  return(x)
}

# inserting our data according to the table provided
matrixd = c(
  0,587,1212,701,1936,604,748,2139,2182,543,
  587,0,920,940,1745,1188,713,1858,1737,597,
  1212,920,0,879,831,1726,1631,949,1021,1494,
  701,940,879,0,1374,968,1420,1645,1891,1220,
  1936,1745,831,1374,0,2339,2451,347,959,2300,
  604,1188,1726,968,2339,0,1092,2594,2734,923,
  748,713,1631,1420,2451,1092,0,2571,2408,205,
  2139,1858,949,1645,347,2594,2571,0,678,2442,
  2182,1737,1021,1891,959,2734,2408,678,0,2329,
  543,597,1494,1220,2300,923,205,2442,2329,0)      # length matrix is 100 , the diagonal is zero's

thematrix<-rename_func(matrixd)  # using the function that we build for the renaming and dimensional fixing

# now we have our table so we can do our analysis (mds and plot 2-d map)
# we will use the euclidean distances in this question

diagg<-diag(nrow(thematrix)) # diagonal matrix

ones_Mat <-matrix(1,nrow = nrow(thematrix),ncol = ncol(thematrix)) # matrix 10x10 with ones (1's)

mtrx1<-(diagg-(1/nrow(thematrix))*ones_Mat)

pre_final_table<-((mtrx1)*ones_Mat) %*% thematrix # matrix multiplication

final_matrix<- -0.5*mtrx1%*%thematrix%*%mtrx1

eigenval<-eigen(final_matrix) # computing the eigenvalues of the final_matrix
eigen_val_values<-eigenval$values # extracting the values of it
eigen_val_vectors<-eigenval$vectors # as well as the vectors
# matrix mult/tion vectors with the diagonal of the sqrt of eigenvalues
the_result<-eigen_val_vectors%*%diag(sqrt(eigen_val_values))  
thetable<-rename_func(the_result)
thetable<-as.data.frame(thetable) # transforming it into data frame

# the final 2-d map is presented below /  we are using first two / we use " - " to rotate the results and make
# more sense / i tried to use many things we learned during lab for the ggplot so i invested some time in ggplot
ggplot(thetable,aes(x = -thetable[,1], y = -thetable[,2],xlim(-40,30),
  label=row.names(thetable)))+labs(title="2-D MAP for Cities",x="First",y="Second")+geom_text()+
  geom_line(aes(colour='magenta'))+guides(colour=FALSE)+theme_bw()+ 
  theme(plot.title = element_text(size = rel(1.5), colour = "blue"),
  axis.text = element_text(colour = "red"))

# the results make some sense as we can see that it seems like
# the cities as representing themselves in the 2-d map as they
# would in a real map of the United States
