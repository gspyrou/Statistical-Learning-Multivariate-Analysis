
library(dplyr)
library(data.table)
library(ggplot2)
library(GGally)
library(MASS)
library(ISLR)
library(sparsediscrim)
library(e1071)
library(reshape2)
require(nnet)     
require(stats)
require(class)
library(gridExtra)

testdata <-read.table("spam-test.txt",sep=",")
traindata <-read.table("spam-train.txt",sep=",")

head(testdata , 3)
head(traindata, 3)

summary(testdata , digits = 3) # summaries for our data
summary(traindata, digits = 3)

testdata<-as.data.frame(testdata)  # transforming them to data frames
traindata<-as.data.frame(traindata) # it makes our life easier
# Now we can devide our data according to several attributes

testdata_centered <- as.data.frame(scale(testdata, center = TRUE, scale = TRUE))
traindata_centered <- as.data.frame(scale(traindata, center = TRUE, scale = TRUE))

colMeans(testdata_centered) # are zeros /  we just checking
colMeans(traindata_centered)

apply(testdata_centered, 2, var) # all variances are unit / again we just checking if it worked
apply(traindata_centered, 2, var)

# (ii) Transform the features using log(xij + 1)

log_testdata <- log(testdata + 1)
log_traindata<- log(traindata + 1)

# (iii) Discretize each features using I(xij > 0)
nonzero_testdata <- lapply(testdata, function(x){replace(x, x > 0, 1)})%>%as.data.frame()
nonzero_traindata <- lapply(traindata, function(x){replace(x, x > 0, 1)})%>%as.data.frame()

# Questions - now we will start constructing different classification rules
# we have to perform LDA,QDA,logistic regressiond and kNN

# A) summaries

#scatter plots
#testdata
ggpairs(testdata, columns=55:57,
        mapping=aes(color=(testdata$V58==1)),
        diag="blank",
        axisLabels = "internal",
        upper=list(continuous='points'))+labs(title="Summary Plots/Scatter test data")
#trainata
ggpairs(traindata, columns=55:57,
        mapping=aes(color=(traindata$V58==1)),
        diag="blank",
        axisLabels = "internal",
        upper=list(continuous='points'))+labs(title="Summary Plots/Scatter train data")

ggplot(testdata, aes(x = testdata$V1, y = testdata$V2, color=(testdata$V58==1))) +
  geom_point() + facet_grid(~testdata$V58)

testdata$V58=as.factor(testdata$V58) # 58 to factor
traindata$V58=as.factor(traindata$V58) # 58 to factor

#test data- coord
ggparcoord(testdata,columns = 1:54,groupColumn =58,scale="globalminmax")+
  labs(title="Coord Plot Test data",x='Features',y="Values",col="Type of email")+theme_bw()+ 
  scale_colour_manual(labels = c("No spam", "Spam"), values = c("darkblue", "red"))
#train data - coord
ggparcoord(traindata,columns = 1:54,groupColumn =58,scale="globalminmax")+
  labs(title="Coord Plot Train data",x='Features',y="Values",col="Type of email")+theme_bw()+ 
  scale_colour_manual(labels = c("No spam", "Spam"), values = c("darkblue", "red"))

#train data - coord- 55-57
ggparcoord(traindata,columns = 55:57,groupColumn =58,scale="globalminmax")+
  labs(title="Coord Plot Train data/Features 55-57",x='Features',y="Values",col="Type of email")+theme_bw()+ 
  scale_colour_manual(labels = c("No spam", "Spam"), values = c("darkblue", "red"))

#test data- coord- 55-57
ggparcoord(testdata,columns = 55:57,groupColumn =58,scale="globalminmax")+
  labs(title="Coord Plot Test data/Features 55-57",x='Features',y="Values",col="Type of email")+theme_bw()+ 
  scale_colour_manual(labels = c("No spam", "Spam"), values = c("darkblue", "red"))


# B) 
# LDA at training data for both standardized and log transformations
testdata <-read.table("spam-test.txt",sep=",")
traindata <-read.table("spam-train.txt",sep=",")

# LDA for standardized data
testdata2 <-testdata[,-58]       # there was also another column number 58 which had only 0's and 1's so
traindata2<-traindata[,-58]      # we can discard this column

testdata_centered <- as.data.frame(scale(testdata2, center = TRUE, scale = TRUE))
traindata_centered <- as.data.frame(scale(traindata2, center = TRUE, scale = TRUE))

testdata_centered$V58 = testdata[,58]
traindata_centered$V58 = traindata[,58]

testdata_centered$V58[testdata_centered$V58 == "1"] <- "Spam"
testdata_centered$V58[testdata_centered$V58 == "0"] <- "No Spam"

traindata_centered$V58[traindata_centered$V58 == "1"] <- "Spam"
traindata_centered$V58[traindata_centered$V58 == "0"] <- "No Spam"

data_lda_stand = lda(data=traindata_centered,traindata_centered$V58~.)
data_predict_stand = predict(data_lda_stand, testdata_centered)
table(testdata_centered$V58 , data_predict_stand$class)  # error rates - here is 43 and 115

# QDA for standardized data 

data_qda_stand = qda(data=traindata_centered,traindata_centered$V58~.)
data_predict_stand_qda = predict(data_qda_stand, testdata_centered)
table(testdata_centered$V58, data_predict_stand_qda$class)   # for standardized

# LDA for log data

log_testdata <- log(testdata2 + 1)
log_traindata<- log(traindata2 + 1)
log_testdata$V58 = testdata[,58]
log_traindata$V58 = traindata[,58]

log_testdata$V58[log_testdata$V58 == "1"] <- "Spam"
log_testdata$V58[log_testdata$V58 == "0"] <- "No Spam"

log_traindata$V58[log_traindata$V58 == "1"] <- "Spam"
log_traindata$V58[log_traindata$V58 == "0"] <- "No Spam"

data_lda_log = lda(data=log_traindata ,log_traindata$V58~.)
data_predict_log = predict(data_lda_log , log_testdata)
table(log_testdata$V58, data_predict_log$class) # error rates -here is 31  and 69

# QDA for log data
data_qda_log = qda(data=log_traindata , log_traindata$V58~.)
data_predict_log_qda = predict(data_qda_log , log_testdata)
table(log_testdata$V58, data_predict_log_qda$class)          # for log

# now we need a plot of the data projected / 2 disc.directions
# we are gonna use the logic of computing orthogonal polynomials

# for LDA / standardized data
disc_direction_1 <-as.numeric(data_lda_stand$scaling[,1])
disc_direction_2<-as.numeric(poly(data_lda_stand$scaling[,1])) / as.numeric(data_lda_stand$scaling[,1])
z1<-as.matrix(traindata_centered[1:57]) %*% scale(disc_direction_1)
z2 <- as.matrix(traindata_centered[1:57]) %*% scale(disc_direction_2)
df <- data.frame(Direction1 = z1 , Direction2=z2, Class_def = as.factor(traindata_centered$V58))

plot_lda<-ggplot(df,aes(x=df$Direction1 , y= df$Direction2))+
  geom_point(data=df , alpha=0.5 ,  aes(pch=Class_def,color = Class_def))+
  labs(title="Data projected / LDA standardized", x="Direction 1" , y= "Direction 2")
plot_lda
##### functions
boundary_plot <- function(df, classifier, predict_function,   resolution = 500) {
  colnames(df) = c("Var1", "Var2", "Class")
  class_train = classifier(x = df[,1:2], y = df[,3])
  v1 = seq(min(df[,1]), max(df[,1]), length=resolution)
  v2 = seq(min(df[,2]), max(df[,2]), length=resolution)
  Grid = expand.grid(Var1 = v1, Var2 = v2)
  Grid$class = predict_function(class_train, Grid)
  ggplot(data=df, aes(x=Var1, y=Var2, color=Class)) +
    geom_contour(data=Grid, aes(z=as.numeric(class)),
                 color="black",size=0.5)+
    geom_point(size=2,aes(color=Class, shape=Class))
}

lda_wrapper = function(x, y) lda(x = x, grouping = y)
predict_wrapper = function(classifier, data) predict(classifier, data)$class

#####functions end

colnames(df) = c("Var1", "Var2", "Class")
class_train  <- lda_wrapper(df[,1:2] , df[,3])
v1 = seq(min(df[,1]), max(df[,1]),length.out = 500)
v2 = seq(min(df[,2]), max(df[,2]),length.out = 500)
Grid = expand.grid(Var1 = v1, Var2 = v2)
Grid$class = predict_wrapper(class_train,Grid)

plot_for_lda<-ggplot(data=df, aes(x=Var1, y=Var2, color=Class)) +
  geom_contour(data=Grid, aes(z=as.numeric(class)),
               color="black",size=0.5)+
  geom_point(size=2,aes(color=Class, shape=Class))+
  theme_bw()+labs(title="Data projected / LDA standardized",x="Direction 1" , y= "Direction 2")
plot_for_lda # final plot for lda standardized data

# plot for LDA /log data
# for LDA / standardized data
disc_direction_1 <-as.numeric(data_lda_log$scaling[,1])
disc_direction_2<-as.numeric(poly(data_lda_log$scaling[,1])) / as.numeric(data_lda_log$scaling[,1])
z1<-as.matrix(log_traindata[1:57]) %*% scale(disc_direction_1)
z2 <- as.matrix(log_traindata[1:57]) %*% scale(disc_direction_2)
df <- data.frame(Direction1 = z1 , Direction2=z2, Class_def = as.factor(log_traindata$V58))

plot_lda<-ggplot(df,aes(x=df$Direction1 , y= df$Direction2))+
  geom_point(data=df , alpha=0.5 ,  aes(pch=Class_def,color = Class_def))+
  labs(title="Data projected / LDA Log", x="Direction 1" , y= "Direction 2")
plot_lda

colnames(df) = c("Var1", "Var2", "Class")
class_train  <- lda_wrapper(df[,1:2] , df[,3])
v1 = seq(min(df[,1]), max(df[,1]),length.out = 500)
v2 = seq(min(df[,2]), max(df[,2]),length.out = 500)
Grid = expand.grid(Var1 = v1, Var2 = v2)
Grid$class = predict_wrapper(class_train,Grid)

function(classifier, data) predict(classifier, data)$class

plot_for_log<-ggplot(data=df, aes(x=Var1, y=Var2, color=Class)) +
  geom_contour(data=Grid, aes(z=as.numeric(class)),
               color="black", linemitre = 1)+
  geom_point(size=2,aes(color=Class, shape=Class))+
  theme_bw()+labs(title="Data projected / LDA Log",x="Direction 1" , y= "Direction 2")
plot_for_log# final plot for lda log data

# QDA plots
# standardized
qda_wrapper = function(x, y) qda(x = x, grouping = y)

disc_direction_1 <-as.numeric(data_qda_stand$scaling[1])
disc_direction_2<-as.numeric(poly(data_qda_stand$scaling[1])) / as.numeric(data_qda_stand$scaling[1])
z1<-as.matrix(traindata_centered[1:57]) %*% scale(disc_direction_1)
z2 <- as.matrix(traindata_centered[1:57]) %*% scale(disc_direction_2)
df <- data.frame(Direction1 = z1 , Direction2=z2, Class_def = as.factor(traindata_centered$V58))

plot_qda_standardized<-ggplot(df,aes(x=df$Direction1 , y= df$Direction2))+
  geom_point(data=df , alpha=0.5 ,  aes(pch=Class_def,color = Class_def))+
  labs(title="Data projected / QDA standardized", x="Direction 1" , y= "Direction 2")
plot_qda_standardized


colnames(df) = c("Var1", "Var2", "Class")
class_train  <- qda_wrapper(df[,1:2] , df[,3])
v1 = seq(min(df[,1]), max(df[,1]),length.out = 500)
v2 = seq(min(df[,2]), max(df[,2]),length.out = 500)
Grid = expand.grid(Var1 = v1, Var2 = v2)
Grid$class = predict_wrapper(class_train,Grid)


plot_for_lda<-ggplot(data=df, aes(x=Var1, y=Var2, color=Class)) +
  geom_contour(data=Grid, aes(z=as.numeric(class)),
               color="black",size=0.5)+
  geom_point(size=2,aes(color=Class, shape=Class))+
  theme_bw()+labs(title="Data projected / QDA standardized",x="Direction 1" , y= "Direction 2")
plot_for_lda # final plot for QDA standardized data

# qda plot / log data
disc_direction_1 <-as.numeric(data_qda_log$scaling[1])
disc_direction_2<-as.numeric(poly(data_qda_log$scaling[1])) / as.numeric(data_qda_log$scaling[1])
z1<-as.matrix(log_traindata[1:57]) %*% scale(disc_direction_1)
z2 <- as.matrix(log_traindata[1:57]) %*% scale(disc_direction_2)
df <- data.frame(Direction1 = z1 , Direction2=z2, Class_def = as.factor(log_traindata$V58))

plot_qda_log<-ggplot(df,aes(x=df$Direction1 , y= df$Direction2))+
  geom_point(data=df , alpha=0.5 ,  aes(pch=Class_def,color = Class_def))+
  labs(title="Data projected / QDA Log", x="Direction 1" , y= "Direction 2")
plot_qda_log


colnames(df) = c("Var1", "Var2", "Class")
class_train  <- qda_wrapper(df[,1:2] , df[,3])
v1 = seq(min(df[,1]), max(df[,1]),length.out = 500)
v2 = seq(min(df[,2]), max(df[,2]),length.out = 500)
Grid = expand.grid(Var1 = v1, Var2 = v2)
Grid$class = predict_wrapper(class_train,Grid)


plot_for_qda<-ggplot(data=df, aes(x=Var1, y=Var2, color=Class)) +
  geom_contour(data=Grid, aes(z=as.numeric(class)),
               color="black",size=0.5)+
  geom_point(size=2,aes(color=Class, shape=Class))+
  theme_bw()+labs(title="Data projected / QDA Log",x="Direction 1" , y= "Direction 2")
plot_for_qda # final plot for QDA log data

# part c
# standardized data / logistic regression
traindata_centered$V58 = as.factor(traindata_centered$V58)
testdata_centered$V58 = as.factor(testdata_centered$V58)

# standardized
logit_original_stand <- glm(V58~., data = traindata_centered ,family='binomial')
summary(logit_original_stand)
head(predict(logit_original_stand, testdata_centered, type='response'))

# test
pred_logit = ifelse(predict(logit_original_stand, testdata_centered, type='response') > 0.5, "Spam" , "No Spam") # test set
testdata_centered$pred_logit = factor(pred_logit, levels=levels(testdata_centered$V58))
test_error_standardized<-mean(testdata_centered$V58 != testdata_centered$pred_logit)%>%as.data.frame() # 0.07170795
# training
pred_logit = ifelse(predict(logit_original_stand, traindata_centered, type='response') > 0.5, "Spam" , "No Spam") # train set
traindata_centered$pred_logit = factor(pred_logit, levels=levels(traindata_centered$V58))
training_error_standardized<-mean(traindata_centered$V58 != traindata_centered$pred_logit)%>%as.data.frame() # 0.07173133

table1<-cbind(test_error_standardized,training_error_standardized)
colnames(table1) = c("mean Test error","mean Training error")
table1

# log transformed data / logistic regression

log_traindata$V58 = as.factor(log_traindata$V58)
log_testdata$V58 = as.factor(log_testdata$V58)

logit_original_log <- glm(V58~. , data = log_traindata ,family='binomial')
summary(logit_original_log)
# test
pred_logit = ifelse(predict(logit_original_log, log_testdata, type='response') > 0.5, "Spam" , "No Spam") # test set
log_testdata$pred_logit = factor(pred_logit, levels=levels(log_testdata$V58))
test_error_log<-mean(log_testdata$V58 != log_testdata$pred_logit)%>%as.data.frame()  #  0.05671447

# training
pred_logit = ifelse(predict(logit_original_log, log_traindata, type='response') > 0.5, "Spam" , "No Spam") # train set
log_traindata$pred_logit = factor(pred_logit, levels=levels(log_traindata$V58))
training_error_log<-mean(log_traindata$V58 != log_traindata$pred_logit)%>%as.data.frame()  # 0.05771112

table1<-cbind(test_error_log,training_error_log)
colnames(table1) = c("mean Test error","mean Training error")
table1

# indicator
nonzero_traindata$V58[nonzero_traindata$V58 == "1"] <- "Spam"
nonzero_traindata$V58[nonzero_traindata$V58 == "0"] <- "No Spam"


nonzero_testdata$V58[nonzero_testdata$V58 == "1"] <- "Spam"
nonzero_testdata$V58[nonzero_testdata$V58 == "0"] <- "No Spam"

nonzero_traindata$V58= as.factor(nonzero_traindata$V58)
nonzero_testdata$V58= as.factor(nonzero_testdata$V58)

logit_original_ind <- glm(nonzero_traindata$V58~. , data = nonzero_traindata ,family='binomial')
summary(logit_original_ind)

#test data
pred_logit = ifelse(predict(logit_original_ind, nonzero_testdata, type='response') > 0.5, "Spam" , "No Spam") # test set
nonzero_testdata$pred_logit = factor(pred_logit, levels=levels(nonzero_testdata$V58))
discretized_error_test<-mean(nonzero_testdata$V58 != nonzero_testdata$pred_logit)%>%as.data.frame()  # 0.08083442

#train data
pred_logit = ifelse(predict(logit_original_ind, nonzero_traindata, type='response') > 0.5, "Spam" , "No Spam") # train set
nonzero_traindata$pred_logit = factor(pred_logit, levels=levels(nonzero_traindata$V58))
discretized_error_train<-mean(nonzero_traindata$V58 != nonzero_traindata$pred_logit)%>%as.data.frame()   # 0.05705902

table1<-cbind(discretized_error_test,discretized_error_train)
colnames(table1) = c("mean Test error","mean Training error")
table1

# part d 
# dimension reduction for pca
# we will use the PCA as suggested from the professor to reduce
# the dimensions

pca = princomp(traindata, cor=TRUE)

scree = qplot(1:length(pca$sdev), pca$sdev, geom='line', ylab='Component SD', xlab='Component')
screeplot(pca) # we will use 3 comp
loadings(pca) # we can see that we can exclude the variables that are not part of the first three components

# trying to find the optimal k for standardized data
reduced_dim_stand = traindata_centered
reduced_dim_stand = reduced_dim_stand[-c(2, 4, 12, 14, 27, 33, 38,44,48)]
knntuning_stand = tune.knn(x= reduced_dim_stand[,1:48], y = reduced_dim_stand$V58, k = 1:49)
summary(knntuning_stand)  # we should use k = 1 # error for this k : 0.06292713 

# trying to find the optimal k for log data
reduced_dim_log = log_traindata
reduced_dim_log = reduced_dim_log[-c(2, 4, 12, 14, 27, 33, 38,44,48)]
knntuning_log = tune.knn(x= reduced_dim_log[,1:48], y = reduced_dim_log$V58,  k = 1:49)
summary(knntuning_log) # we should use k = 1  # error for this k : 0.05314449 

# trying to find the optimal k for discritized data
reduced_dim_disc = nonzero_traindata
reduced_dim_disc = reduced_dim_disc[-c(2, 4, 12, 14, 27, 33, 38,44,48)]
knntuning_disc = tune.knn(x= reduced_dim_disc[,1:48], y = reduced_dim_disc$V58,  k = 1:49)
summary(knntuning_disc ) # we should use k = 1 # error for this k : 0.04759852

# standardized data

reduced_dim_stand_test = testdata_centered
reduced_dim_stand_test = reduced_dim_stand_test[-c(2, 4, 12, 14, 27, 33, 38,44,48)]

folds = cv_partition(reduced_dim_stand$V58, num_folds = 5)

train_cv_error = function(K) {
  #Train error
  auto.knn = knn(train = reduced_dim_stand[,1:48], test = reduced_dim_stand[,1:48], 
                 cl = reduced_dim_stand$V58, k = K)
  train_error = sum(auto.knn != reduced_dim_stand$V58) / nrow(reduced_dim_stand)
  #CV error
  auto.cverr = sapply(folds, function(fold) {
    sum(reduced_dim_stand$V58[fold$test] != knn(train = reduced_dim_stand[fold$training,1:48], cl = reduced_dim_stand[fold$training,49], test = reduced_dim_stand[fold$test, 1:48], k=K)) / length(fold$test)
  })
  cv_error = mean(auto.cverr)
  #Test error
  auto.knn.test = knn(train = reduced_dim_stand[,1:48], test = reduced_dim_stand_test[,1:48], 
                      cl = reduced_dim_stand$V58, k = K)
  test_error = sum(auto.knn.test != reduced_dim_stand_test$V58) / nrow(reduced_dim_stand_test)
  return(c(train_error, cv_error, test_error))
}
auto_k_errors = sapply(1:48, function(k) train_cv_error(k))
df_errs = data.frame(t(auto_k_errors), 1:48)
colnames(df_errs) = c('Train', 'CV', 'Test', 'K')

dataL <- melt(df_errs, id="K")
ggplot(dataL, aes_string(x="K", y="value", colour="variable",
                         group="variable", linetype="variable", shape="variable")) +
  geom_line(size=1) + labs(title='Standardized data' ,x = "Number of nearest neighbors",
                           y = "Classification error",
                           colour="",group="",
                           linetype="",shape="") +
  geom_point(size=4)+theme_bw()

# log data
# 

reduced_dim_log_test = log_testdata
reduced_dim_log_test = reduced_dim_log_test[-c(2, 4, 12, 14, 27, 33, 38,44,48)]

folds = cv_partition(reduced_dim_log$V58, num_folds = 5)

train_cv_error = function(K) {
  #Train error
  auto.knn = knn(train = reduced_dim_log[,1:48], test = reduced_dim_log[,1:48], 
                 cl = reduced_dim_log$V58, k = K)
  train_error = sum(auto.knn != reduced_dim_log$V58) / nrow(reduced_dim_log)
  #CV error
  auto.cverr = sapply(folds, function(fold) {
    sum(reduced_dim_log$V58[fold$test] != knn(train = reduced_dim_log[fold$training,1:48], cl = reduced_dim_log[fold$training,49], test = reduced_dim_log[fold$test, 1:48], k=K)) / length(fold$test)
  })
  cv_error = mean(auto.cverr)
  #Test error
  auto.knn.test = knn(train = reduced_dim_log[,1:48], test = reduced_dim_log_test[,1:48], 
                      cl = reduced_dim_log$V58, k = K)
  test_error = sum(auto.knn.test != reduced_dim_log_test$V58) / nrow(reduced_dim_log_test)
  return(c(train_error, cv_error, test_error))
}
auto_k_errors = sapply(1:48, function(k) train_cv_error(k))
df_errs = data.frame(t(auto_k_errors), 1:48)
colnames(df_errs) = c('Train', 'CV', 'Test', 'K')

dataL <- melt(df_errs, id="K")
ggplot(dataL, aes_string(x="K", y="value", colour="variable",
                         group="variable", linetype="variable", shape="variable")) +
  geom_line(size=1) + labs(title='Log data' ,x = "Number of nearest neighbors",
                           y = "Classification error",
                           colour="",group="",
                           linetype="",shape="") +
  geom_point(size=4)+theme_bw()

# discretized data

reduced_dim_disc_test = nonzero_testdata
reduced_dim_disc_test = reduced_dim_disc_test[-c(2, 4, 12, 14, 27, 33, 38,44,48)]

folds = cv_partition(reduced_dim_disc$V58, num_folds = 5)

train_cv_error = function(K) {
  #Train error
  auto.knn = knn(train = reduced_dim_disc[,1:48], test = reduced_dim_disc[,1:48], 
                 cl = reduced_dim_disc$V58, k = K)
  train_error = sum(auto.knn != reduced_dim_disc$V58) / nrow(reduced_dim_disc)
  #CV error
  auto.cverr = sapply(folds, function(fold) {
    sum(reduced_dim_disc$V58[fold$test] != knn(train = reduced_dim_disc[fold$training,1:48], cl = reduced_dim_disc[fold$training,49], test = reduced_dim_disc[fold$test, 1:48], k=K)) / length(fold$test)
  })
  cv_error = mean(auto.cverr)
  #Test error
  auto.knn.test = knn(train = reduced_dim_disc[,1:48], test = reduced_dim_disc_test[,1:48], 
                      cl = reduced_dim_disc$V58, k = K)
  test_error = sum(auto.knn.test != reduced_dim_disc_test$V58) / nrow(reduced_dim_disc_test)
  return(c(train_error, cv_error, test_error))
}
auto_k_errors = sapply(1:48, function(k) train_cv_error(k))
df_errs = data.frame(t(auto_k_errors), 1:48)
colnames(df_errs) = c('Train', 'CV', 'Test', 'K')

dataL <- melt(df_errs, id="K")
ggplot(dataL, aes_string(x="K", y="value", colour="variable",
                         group="variable", linetype="variable", shape="variable")) +
  geom_line(size=1) + labs(title='Discretized data' ,x = "Number of nearest neighbors",
                           y = "Classification error",
                           colour="",group="",
                           linetype="",shape="") +
  geom_point(size=4)+theme_bw()

# e) training and test errors for each of the two classes separately
# part e
# standardized / lda 

data_predict_stand_2 = predict(data_lda_stand, traindata_centered)
table(testdata_centered$V58, data_predict_stand$class) 
mean(testdata_centered$V58 != data_predict_stand$class) 
table(traindata_centered$V58, data_predict_stand_2$class) 
mean(traindata_centered$V58 != data_predict_stand_2$class) 
# log transformed / lda
data_predict_log_2 = predict(data_lda_log, traindata_centered)
table(log_testdata$V58, data_predict_log$class) 
mean(log_testdata$V58 != data_predict_log$class) 

table(log_traindata$V58, data_predict_log_2$class) 
mean(log_traindata$V58 != data_predict_log_2$class) 

# standardized / qda 

data_predict_stand_qda_2= predict(data_qda_stand, traindata_centered)
table(testdata_centered$V58, data_predict_stand_qda$class) 
mean(testdata_centered$V58 != data_predict_stand_qda$class) 
table(traindata_centered$V58, data_predict_stand_qda_2$class) 
mean(traindata_centered$V58 != data_predict_stand_qda_2$class) 
# log  / qda 
data_predict_log_qda_2= predict(data_qda_log, traindata_centered)
table(testdata_centered$V58, data_predict_log_qda$class) 
mean(testdata_centered$V58 != data_predict_log_qda$class) 

table(traindata_centered$V58, data_predict_log_qda_2$class) 
mean(traindata_centered$V58 != data_predict_log_qda_2$class) 
# knn standardized
std.knn.train = knn(train = reduced_dim_stand[,1:48], test = reduced_dim_stand[,1:48], 
                    cl = reduced_dim_stand$V58, k = 2)
table(std.knn.train, reduced_dim_stand$V58) 
mean(std.knn.train != reduced_dim_stand$V58)
std.knn.test =knn(train = reduced_dim_stand[,1:48], test = reduced_dim_stand_test[,1:48], 
                  cl = reduced_dim_stand$V58, k = 2)

table(std.knn.test, reduced_dim_stand_test$V58) 
mean(std.knn.test != reduced_dim_stand_test$V58)
# knn log
log.knn.train = knn(train = reduced_dim_log[,1:48], test = reduced_dim_log[,1:48], 
                    cl = reduced_dim_log$V58, k = 2)

table(log.knn.train, reduced_dim_log$V58) 
mean(log.knn.train != reduced_dim_log$V58)
log.knn.test =knn(train = reduced_dim_log[,1:48], test = reduced_dim_log_test[,1:48], 
                  cl = reduced_dim_log$V58, k = 2)
table(log.knn.test, reduced_dim_log_test$V58) 
mean(log.knn.test != reduced_dim_log_test$V58)
# knn disc
disc.knn.train = knn(train = reduced_dim_disc[,1:48], test = reduced_dim_disc[,1:48], 
                     cl = reduced_dim_disc$V58, k = 2)

table(disc.knn.train , reduced_dim_disc$V58) 
mean(disc.knn.train  != reduced_dim_disc$V58)
disc.knn.test =knn(train = reduced_dim_disc[,1:48], test = reduced_dim_disc_test[,1:48], 
                   cl = reduced_dim_disc$V58, k = 2)
table(disc.knn.test, reduced_dim_disc_test$V58) 
mean(disc.knn.test != reduced_dim_disc_test$V58)
